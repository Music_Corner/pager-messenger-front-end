export const TOKEN_NAMES = {
  ACCESS: 'accessToken',
  REFRESH: 'refreshToken',
};

export const saveTokens = ({ accessToken, refreshToken }) => {
  console.log('saving tokens', accessToken, refreshToken);
  if (accessToken) {
    localStorage.setItem(TOKEN_NAMES.ACCESS, accessToken);
  }

  if (refreshToken) {
    localStorage.setItem(TOKEN_NAMES.REFRESH, refreshToken);
  }
};

export const getTokens = (name = false) => {
  const accessToken = localStorage.getItem(TOKEN_NAMES.ACCESS);

  if (!accessToken) {
    console.log('returning null');
    return null;
  }


  return name ? localStorage.getItem(name) : {
    accessToken: localStorage.getItem(TOKEN_NAMES.ACCESS),
    refreshToken: localStorage.getItem(TOKEN_NAMES.REFRESH),
  };
};

export const removeTokens = () => {
  localStorage.removeItem(TOKEN_NAMES.ACCESS);
  localStorage.removeItem(TOKEN_NAMES.REFRESH);
};
