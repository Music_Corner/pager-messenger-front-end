export const HEADER_TITLES_CONSTANTS = {
  GET_DIALOGUE_TITLE: (firstName, lastName) => `${firstName} ${lastName}`,
  PROFILE: 'Profile',
  DIALOGUES: 'Dialogues',
  NEW_DIALOGUE: 'New dialogue',
};
