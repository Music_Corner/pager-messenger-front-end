export const ROUTES = {
  LOGIN: '/login',
  SIGN_UP: '/sign-up',
  EMAIL_VERIFICATION: '/verify/:verificationHash',
  HOME: '/dialogues',
  DIALOGUE: '/dialogues/:id',
  GET_DIALOGUE_ROUTE: (id) => `/dialogues/${id}`,
  PROFILE: '/profile',
  START_DIALOGUE: '/start-dialogue',
  USER: '/user',
  GET_USER_ROUTE(id) { return `${this.USER}/${id}`; },
};
