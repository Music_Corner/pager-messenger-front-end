
const PORT = 9615;
const MAIN_API_URL = '192.168.0.3';

export const API_CONFIG = {
  PORT,
  URL: MAIN_API_URL,
  LINK: `${MAIN_API_URL}:${PORT}`,
};

export const API_METHODS = {
  SIGN_UP: '/signup',
  EMAIL_VERIFICATION: '/email-verify',
  CHECK_AUTH: '/current',
  LOGIN: '/login',
  LOGOUT: '/logout',
  USERS: '/users',
};
