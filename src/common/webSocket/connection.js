import { API_CONFIG } from '../constants/api/api';

const initWebSocket = () => new WebSocket(`ws://${API_CONFIG.LINK}`);

export default initWebSocket;
