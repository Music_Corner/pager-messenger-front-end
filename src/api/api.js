import axios from 'axios';
import { API_CONFIG } from '../common/constants/api/api';
import { TOKEN_NAMES, getTokens } from '../common/constants/tokens/tokens';

const initApi = () => {
  axios.defaults.headers.common['Auth-Access'] = getTokens(TOKEN_NAMES.ACCESS);

  const axiosInstance = axios.create({
    baseURL: `http://${API_CONFIG.LINK}`,
  });

  const defaultSuccessHandler = (data) => data;

  const defaultErrorHandler = (e) => { throw e.response.data; };

  const api = {
    ...axiosInstance,
    get: (...args) => (
      axiosInstance.get(...args).then(defaultSuccessHandler).catch(defaultErrorHandler)
    ),
    post: (...args) => (
      axiosInstance.post(...args).then(defaultSuccessHandler).catch(defaultErrorHandler)
    ),
  };

  return api;
};

export default initApi;
