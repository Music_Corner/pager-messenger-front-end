import initApi from '../api';

export const dialoguesApi = {
  get: (page = 0) => initApi().get(`/dialogues/?page=${page}`).then(res => res.data.data),
};
