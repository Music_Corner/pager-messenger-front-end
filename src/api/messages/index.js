import initApi from '../api';

const messagesApi = {
  getMessages: (id) => initApi().get(`/dialogue/?id=${id}`),
  postMessage: (data) => initApi().post('/message', data),
};

export default messagesApi;
