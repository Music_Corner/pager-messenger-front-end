import initApi from 'src/api/api';
import { API_METHODS } from 'src/common/constants/api/api';

export const usersApi = {
  getUsers: (serachParams = {}) => initApi().get(API_METHODS.USERS, { params: serachParams }),
};
