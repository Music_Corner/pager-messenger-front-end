import { API_METHODS } from 'src/common/constants/api/api';
import { getTokens, TOKEN_NAMES } from '../../common/constants/tokens/tokens';
import initApi from '../api';

const profileApi = {
  postSignUp: (data) => initApi().post(API_METHODS.SIGN_UP, data),
  verifyEmail: (verification) => initApi().get(API_METHODS.EMAIL_VERIFICATION, { verification }),
  checkAuth: (refresh = false) => initApi().get(API_METHODS.CHECK_AUTH, {
    headers: refresh ? {
      'Auth-Refresh': getTokens(TOKEN_NAMES.REFRESH),
    } : {},
  }),
  login: (data) => initApi().post(API_METHODS.LOGIN, data),
  logout: () => initApi().get(API_METHODS.LOGOUT),
};

export default profileApi;
