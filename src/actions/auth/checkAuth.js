import { getDefaultFetchActionNames } from '../common/actionsExtension';

const CHECK_AUTH = 'CHECK_AUTH';

export const CHECK_AUTH_ACTIONS = getDefaultFetchActionNames(CHECK_AUTH);

export const checkAuthActions = {
  request: (history, showError) => ({ type: CHECK_AUTH_ACTIONS.REQUEST, history, showError }),
  success: (payload) => ({ type: CHECK_AUTH_ACTIONS.SUCCESS, payload }),
  error: (payload) => ({ type: CHECK_AUTH_ACTIONS.ERROR, payload }),
};
