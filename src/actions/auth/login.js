import { getDefaultFetchActionNames } from '../common/actionsExtension';

const FETCH_LOGIN = 'FETCH_LOGIN';

export const FETCH_LOGIN_ACTIONS = getDefaultFetchActionNames(FETCH_LOGIN);

export const fetchLoginActions = {
  request: (payload, history) => ({ type: FETCH_LOGIN_ACTIONS.REQUEST, payload, history }),
  success: (payload) => ({ type: FETCH_LOGIN_ACTIONS.SUCCESS, payload }),
  error: (payload) => ({ type: FETCH_LOGIN_ACTIONS.ERROR, payload }),
};
