import { getDefaultFetchActionNames } from '../common/actionsExtension';

const LOGOUT_ACTION = 'FETCH_LOGOUT';

export const LOGOUT_ACTIONS = getDefaultFetchActionNames(LOGOUT_ACTION);

export const logoutActions = {
  request: (history) => ({ type: LOGOUT_ACTIONS.REQUEST, history }),
  success: (payload) => ({ type: LOGOUT_ACTIONS.SUCCESS, payload }),
  error: (payload) => ({ type: LOGOUT_ACTIONS.ERROR, payload }),
};
