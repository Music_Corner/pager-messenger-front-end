import { getDefaultFetchActionNames } from '../common/actionsExtension';

const EMAIL_VERIFICATION = 'EMAIL_VERIFICATION';

export const EMAIL_VERIFICATION_ACTIONS = getDefaultFetchActionNames(EMAIL_VERIFICATION);

export const emailVerificationActions = {
  request: (payload) => ({ type: EMAIL_VERIFICATION_ACTIONS.REQUEST, payload }),
  success: (payload) => ({ type: EMAIL_VERIFICATION_ACTIONS.SUCCESS, payload }),
  error: (payload) => ({ type: EMAIL_VERIFICATION_ACTIONS.ERROR, payload }),
};
