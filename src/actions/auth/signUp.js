import { getDefaultFetchActionNames } from '../common/actionsExtension';

const SIGN_UP_ACTION = 'SIGN_UP';

export const SIGN_UP_ACTIONS = getDefaultFetchActionNames(SIGN_UP_ACTION);

export const signUpFetchActions = {
  request: (payload) => ({ type: SIGN_UP_ACTIONS.REQUEST, payload }),
  success: (payload) => ({ type: SIGN_UP_ACTIONS.SUCCESS, payload }),
  error: (payload) => ({ type: SIGN_UP_ACTIONS.ERROR, payload }),
};
