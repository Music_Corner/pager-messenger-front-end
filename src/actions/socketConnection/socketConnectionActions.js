import { getDefaultFetchActionNames } from '../common/actionsExtension';

const SOCKET_ACTIONS = 'SOCKET_CONNECTION';

export const SOCKET_CONNECTION = getDefaultFetchActionNames(SOCKET_ACTIONS);
export const socketConnectionActions = {
  request: (payload) => ({ type: SOCKET_CONNECTION.REQUEST, payload }),
  success: (payload) => ({ type: SOCKET_CONNECTION.SUCCESS, payload }),
  error: (payload) => ({ type: SOCKET_CONNECTION.ERROR, payload }),
};
