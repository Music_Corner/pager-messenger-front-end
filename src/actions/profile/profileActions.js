export const PROFILE_ACTIONS = {
  FULFILL_PROFILE_INFO: 'FULFILL_PROFILE_INFO',
  CLEAR_PROFILE_INFO: 'CLEAR_PROFILE_INFO',
};

export const profileActions = {
  fulfillProfileInfo: (payload) => ({ type: PROFILE_ACTIONS.FULFILL_PROFILE_INFO, payload }),
  clearProfileInfo: () => ({ type: PROFILE_ACTIONS.CLEAR_PROFILE_INFO }),
};
