export const ON_MESSAGE = 'ON_MESSAGE';

const onMessageAction = (payload) => ({
  type: ON_MESSAGE,
  payload,
});

export default onMessageAction;
