import { getDefaultFetchActionNames } from '../common/actionsExtension';

const GET_MESSAGES = 'GET_MESSAGES';

export const GET_MESSAGES_ACTIONS = getDefaultFetchActionNames(GET_MESSAGES);

export const getMessagesActions = {
  request: (payload) => ({ type: GET_MESSAGES_ACTIONS.REQUEST, payload }),
  success: (payload) => ({ type: GET_MESSAGES_ACTIONS.SUCCESS, payload }),
  error: (payload) => ({ type: GET_MESSAGES_ACTIONS.ERROR, payload }),
};
