const CHANGE_MESSAGE_FORM = 'CHANGE_MESSAGE_FORM';

export const CHANGE_MESSAGE_FORM_ACTIONS = {
  CONTENT: `${CHANGE_MESSAGE_FORM}_CONTENT`,
  ADD_ATTACHMENTS: `${CHANGE_MESSAGE_FORM}_ADD_ATTACHMENTS`,
  DELETE_ATTACHMENT: `${CHANGE_MESSAGE_FORM}_DELETE_ATTACHMENT`,
  FROM_TO: `${CHANGE_MESSAGE_FORM}_FROM_TO`,
  CLEAR: `${CHANGE_MESSAGE_FORM}_CLEAR`,
};


export const changeMessageFormActions = {
  content: (payload) => ({
    type: CHANGE_MESSAGE_FORM_ACTIONS.CONTENT,
    payload,
  }),
  addAttachments: (files, type) => ({
    type: CHANGE_MESSAGE_FORM_ACTIONS.ADD_ATTACHMENTS,
    payload: Array.from(files).map(file => ({ file, type })),
  }),
  deleteAttachment: (payload) => ({
    type: CHANGE_MESSAGE_FORM_ACTIONS.DELETE_ATTACHMENT,
    payload,
  }),
  fromTo: (from, to) => ({
    type: CHANGE_MESSAGE_FORM_ACTIONS.FROM_TO,
    payload: { from, to },
  }),
  clear: () => ({ type: CHANGE_MESSAGE_FORM_ACTIONS.CLEAR }),
};
