import { getDefaultFetchActionNames } from "src/actions/common/actionsExtension";

const SEND_MESSAGE = 'SEND_MESSAGE';

export const SEND_MESSAGE_ACTIONS = getDefaultFetchActionNames(SEND_MESSAGE);

const getCurrentDataToSend = () => (
  require('src/common/store/store').default.getState().messages.form.dataToSend
);

export const sendMessageActions = {
  request: () => ({ type: SEND_MESSAGE_ACTIONS.REQUEST, payload: getCurrentDataToSend() }),
  error: (payload) => ({ type: SEND_MESSAGE_ACTIONS.ERROR, payload }),
  success: (payload) => ({ type: SEND_MESSAGE_ACTIONS.SUCCESS, payload }),
};
