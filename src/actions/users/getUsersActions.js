import { getDefaultFetchActionNames } from 'src/actions/common/actionsExtension';

const GET_USERS_ACTION_NAME = 'GET_USERS';

export const GET_USERS_ACTIONS_NAMES = getDefaultFetchActionNames(GET_USERS_ACTION_NAME);

export const getUsersActions = {
  request: (payload) => ({ type: GET_USERS_ACTIONS_NAMES.REQUEST, payload }),
  success: (payload) => ({ type: GET_USERS_ACTIONS_NAMES.SUCCESS, payload }),
  error: (payload) => ({ type: GET_USERS_ACTIONS_NAMES.ERROR, payload }),
};
