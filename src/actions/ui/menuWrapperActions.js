export const MENU_WRAPPER_ACTIONS = {
  SET_HEADER_TITLE: 'SET_HEADER_TITLE',
};

export const menuWrapperActions = {
  setHeaderTitle: (payload) => ({
    type: MENU_WRAPPER_ACTIONS.SET_HEADER_TITLE,
    payload,
  }),
};
