export const getDefaultFetchActionNames = (ACTION) => ({
  REQUEST: `${ACTION}_REQUEST`,
  SUCCESS: `${ACTION}_SUCCESS`,
  ERROR: `${ACTION}_ERROR`,
});
