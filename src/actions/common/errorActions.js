export const TOGGLE_ERROR_MODAL = 'TOGGLE_ERROR_MODAL';

export const toggleErrorModal = (payload) => ({
  payload,
  type: TOGGLE_ERROR_MODAL,
});
