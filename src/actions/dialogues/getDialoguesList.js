import { getDefaultFetchActionNames } from '../common/actionsExtension';

const FETCH_DIALOGUES_LIST = 'FETCH_DIALOGUES_LIST';

export const FETCH_DIALOGUES_LIST_ACTIONS = getDefaultFetchActionNames(FETCH_DIALOGUES_LIST);

export const fetchDialoguesListActions = {
  request: (payload) => ({ type: FETCH_DIALOGUES_LIST_ACTIONS.REQUEST, payload }),
  error: (payload) => ({ type: FETCH_DIALOGUES_LIST_ACTIONS.ERROR, payload }),
  success: (payload) => ({ type: FETCH_DIALOGUES_LIST_ACTIONS.SUCCESS, payload }),
};
