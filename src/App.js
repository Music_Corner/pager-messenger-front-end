import 'regenerator-runtime/runtime';
import React from 'react';
import { Provider } from 'react-redux';

import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

import ErrorHandleContainer from './containers/Common/ErrorHandle/ErrorHandleContainer';
import { RootRouter } from './routers/RootRouter';
import store from './common/store/store';

const App = () => (
  <Provider store={store}>
    <ErrorHandleContainer />
    <RootRouter />
  </Provider>
);

export default App;
