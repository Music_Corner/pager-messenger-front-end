import { GET_USERS_ACTIONS_NAMES } from 'src/actions/users/getUsersActions';

const initialState = {
  isPending: false,
  data: [],
  error: false,
};

const usersReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_USERS_ACTIONS_NAMES.REQUEST:
      return {
        ...state,
        isPending: true,
      };

    case GET_USERS_ACTIONS_NAMES.SUCCESS:
      return {
        ...state,
        data: payload,
        isPending: false,
      };

    case GET_USERS_ACTIONS_NAMES.ERROR:
      return {
        ...state,
        isPending: false,
        error: payload,
      };

    default:
      return state;
  }
};

export default usersReducer;
