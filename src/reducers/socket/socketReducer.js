import { SOCKET_CONNECTION } from '../../actions/socketConnection/socketConnectionActions';

const initialState = {
  connection: null,
  isConnecting: false,
};

const socketReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SOCKET_CONNECTION.REQUEST:
      return {
        ...state,
        isConnecting: true,
      };

    case SOCKET_CONNECTION.SUCCESS:
      return {
        ...state,
        isConnecting: false,
        connection: payload,
      };

    default:
      return state;
  }
};

export default socketReducer;
