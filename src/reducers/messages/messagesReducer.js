import { uuid } from 'uuidv4';

import { GET_MESSAGES_ACTIONS } from '../../actions/messages/getMessages';
import { ON_MESSAGE } from '../../actions/messages/onMessage';
import { SEND_MESSAGE_ACTIONS } from 'src/actions/messages/sendMessage';
import { CHANGE_MESSAGE_FORM_ACTIONS } from 'src/actions/messages/changeFormActions';
import { getUniqueArray } from 'src/common/constants/functions/index';

const initialState = {
  data: [],
  isPending: false,
  form: {
    isPending: false,
    error: null,
    dataToSend: {
      from: null,
      to: null,
      content: '',
      is_audio: false,
      attachments: [],
    },
  },
};

const getUniqueMessagesArray = (data) => getUniqueArray(data, 'id');

const messagesReducer = (state = initialState, { payload, type }) => {
  switch (type) {
    case GET_MESSAGES_ACTIONS.REQUEST:
      return {
        ...state,
        isPending: true,
      };

    case GET_MESSAGES_ACTIONS.SUCCESS:
      return {
        ...state,
        isPending: false,
        data: payload,
      };

    case ON_MESSAGE:
    case SEND_MESSAGE_ACTIONS.SUCCESS:
      return {
        ...state,
        data: getUniqueMessagesArray([...state.data, payload]),
      };

    case SEND_MESSAGE_ACTIONS.REQUEST:
      return {
        ...state,
        form: {
          ...state.form,
          isPending: true,
        },
      };

    case SEND_MESSAGE_ACTIONS.ERROR:
      return {
        ...state,
        form: {
          ...state.form,
          isPending: false,
          error: payload,
        },
      };

    case CHANGE_MESSAGE_FORM_ACTIONS.CONTENT:
      return {
        ...state,
        form: {
          ...state.form,
          dataToSend: {
            ...state.form.dataToSend,
            content: payload,
          },
        },
      };

    case CHANGE_MESSAGE_FORM_ACTIONS.ADD_ATTACHMENTS:
      return {
        ...state,
        form: {
          ...state.form,
          dataToSend: {
            ...state.form.dataToSend,
            attachments: [
              ...state.form.dataToSend.attachments,
              ...payload.map(attachment => ({ ...attachment, id: uuid() })),
            ],
          },
        },
      };

    case CHANGE_MESSAGE_FORM_ACTIONS.DELETE_ATTACHMENT:
      return {
        ...state,
        form: {
          ...state.form,
          dataToSend: {
            ...state.form.dataToSend,
            attachments: [...state.form.dataToSend.attachments]
              .filter(({ id }) => id !== payload),
          },
        },
      };

    case CHANGE_MESSAGE_FORM_ACTIONS.CLEAR:
      return {
        ...state,
        form: {
          ...state.form,
          dataToSend: {
            ...initialState.form.dataToSend,
            from: state.form.dataToSend.from,
            to: state.form.dataToSend.to,
          },
        },
      };

    case CHANGE_MESSAGE_FORM_ACTIONS.FROM_TO:
      return {
        ...state,
        form: {
          ...state.form,
          dataToSend: {
            ...state.form.dataToSend,
            ...payload,
          },
        },
      };

    default:
      return state;
  }
};

export default messagesReducer;
