import { MENU_WRAPPER_ACTIONS } from 'src/actions/ui/menuWrapperActions';

const initialState = {
  header: {
    title: 'Home',
    backButtonIsShown: true,
  },
};

const uiReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case MENU_WRAPPER_ACTIONS.SET_HEADER_TITLE:
      return {
        ...state,
        header: {
          ...state.header,
          title: payload,
        },
      };

    default:
      return state;
  }
};

export default uiReducer;
