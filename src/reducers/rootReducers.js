import { combineReducers } from 'redux';
import profileReducer from './profile/profile';
import errorHandleReducer from './common/errorHandleReducer';
import messagesReducer from './messages/messagesReducer';
import socketReducer from './socket/socketReducer';
import dialoguesReducer from './dialogues/dialoguesReducer';
import uiReducer from 'src/reducers/ui/ui';
import authReducer from 'src/reducers/auth/auth';
import usersReducer from 'src/reducers/users/index';

const rootReducers = combineReducers({
  profile: profileReducer,
  error: errorHandleReducer,
  messages: messagesReducer,
  socketConnection: socketReducer,
  dialogues: dialoguesReducer,
  ui: uiReducer,
  auth: authReducer,
  users: usersReducer,
});

export default rootReducers;
