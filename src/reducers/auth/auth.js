import { EMAIL_VERIFICATION_ACTIONS } from "src/actions/auth/emailVerification";
import { CHECK_AUTH_ACTIONS } from "src/actions/auth/checkAuth";
import { FETCH_LOGIN_ACTIONS } from "src/actions/auth/login";
import { SIGN_UP_ACTIONS } from "src/actions/auth/signUp";
import { LOGOUT_ACTIONS } from "src/actions/auth/logout";

const initialState = {
  isPending: false,
  isAuthenticated: false,
  emailVerificationError: null,
  emailVerified: false,
};

const authReducer = (state = initialState, { payload, type }) => {
  switch (type) {
    case SIGN_UP_ACTIONS.REQUEST:
    case FETCH_LOGIN_ACTIONS.REQUEST:
    case CHECK_AUTH_ACTIONS.REQUEST:
      return {
        ...state,
        isPending: true,
      };

    case LOGOUT_ACTIONS.SUCCESS:
      return initialState;

    case SIGN_UP_ACTIONS.SUCCESS:
      return {
        ...state,
        isPending: false,
      };

    case SIGN_UP_ACTIONS.ERROR:
    case FETCH_LOGIN_ACTIONS.ERROR:
    case CHECK_AUTH_ACTIONS.ERROR:
      return {
        ...state,
        isPending: false,
        error: payload,
      };

    case EMAIL_VERIFICATION_ACTIONS.SUCCESS:
    case CHECK_AUTH_ACTIONS.SUCCESS:
      return {
        ...state,
        emailVerified: true,
        isPending: false,
        isAuthenticated: true,
      };

    case EMAIL_VERIFICATION_ACTIONS.ERROR:
      return {
        ...state,
        emailVerified: false,
        emailVerificationError: true,
        isAuthenticated: false,
      };

    case FETCH_LOGIN_ACTIONS.SUCCESS: {
      return {
        ...state,
        isAuthenticated: true,
        isPending: false,
      };
    }

    default:
      return state;
  }
};

export default authReducer;
