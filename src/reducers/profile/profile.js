import { PROFILE_ACTIONS } from 'src/actions/profile/profileActions';

const initialState = null;

const profileReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case PROFILE_ACTIONS.FULFILL_PROFILE_INFO:
      return {
        ...state,
        ...payload,
      };

    case PROFILE_ACTIONS.CLEAR_PROFILE_INFO:
      return initialState;

    default:
      return state;
  }
};

export default profileReducer;
