import { FETCH_DIALOGUES_LIST_ACTIONS } from "../../actions/dialogues/getDialoguesList";

const initialState = {
  data: [],
  isPending: false,
};

const dialoguesReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_DIALOGUES_LIST_ACTIONS.REQUEST:
      return {
        ...state,
        isPending: true,
      };

    case FETCH_DIALOGUES_LIST_ACTIONS.SUCCESS:
      return {
        ...state,
        data: payload,
        isPending: false,
      };

    case FETCH_DIALOGUES_LIST_ACTIONS.ERROR:
      return {
        ...state,
        isPending: false,
      };

    default:
      return state;
  }
};

export default dialoguesReducer;
