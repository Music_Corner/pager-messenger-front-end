import { TOGGLE_ERROR_MODAL } from '../../actions/common/errorActions';

const initialState = {
  error: null,
  errorModal: {
    isToggled: false,
    errorMessage: '',
  },
};

const errorHandleReducer = (state = initialState, { payload, type }) => {
  switch (type) {
    case TOGGLE_ERROR_MODAL:
      return {
        ...state,
        errorModal: {
          ...state.errorModal,
          isToggled: !state.errorModal.isToggled,
          errorMessage: payload,
        },
        error: !state.errorModal.isToggled,
      };

    default:
      return state;
  }
};

export default errorHandleReducer;
