import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import UsersListItemContainer from 'src/containers/SearchUsers/Users/UsersListItemContainer/index';
import Loader from 'src/components/Common/Loader/Loader';
import { commonPropTypes } from 'src/common/constants/propTypes/propTypesConstants';

import styles from './index.scss';

const UsersList = ({ data, isPending, setHeaderTitle }) => {
  useEffect(() => {
    setHeaderTitle();
  }, [0]);

  return (
    <div className={styles.usersList}>
      {isPending ? (
        <Loader />
      ) : (
        <div className={styles.usersListMap}>
          {data.map(({ id }, index) => <UsersListItemContainer key={id} index={index} />)}
        </div>
      )}
    </div>
  );
};

UsersList.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape(commonPropTypes.commonUserInfo)),
  isPending: PropTypes.bool.isRequired,
  setHeaderTitle: PropTypes.func.isRequired,
};

export default UsersList;
