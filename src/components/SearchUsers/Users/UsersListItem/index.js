import React from 'react';

import AvatarImage from 'src/components/Common/AvatarImage/AvatarImage';
import { Link } from 'react-router-dom';
import { ROUTES } from 'src/common/constants/routes/routes';
import MainButton from 'src/components/Common/Buttons/MainButton';
import styles from './index.scss';
import { commonPropTypes } from 'src/common/constants/propTypes/propTypesConstants';

const UsersListItem = ({ id, avatarLink, firstName, lastName }) => (
  <div className={styles.user}>
    <div className={styles.user_avatar}>
      <AvatarImage imageUrl={avatarLink} />
    </div>

    <div className={styles.user_info}>
      <Link to={ROUTES.GET_USER_ROUTE(id)}>
        {firstName} {lastName}
      </Link>
    </div>

    <div className={styles.user_actions}>
      <Link to={ROUTES.GET_DIALOGUE_ROUTE(id)}>
        <MainButton className={styles.messageButton}>
          <i className="fas fa-paper-plane" />
        </MainButton>
      </Link>
    </div>
  </div>
);

UsersListItem.propTypes = commonPropTypes.commonUserInfo;

export default UsersListItem;
