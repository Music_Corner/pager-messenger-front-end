import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import MainInput from 'src/components/Common/Inputs/MainInput/MainInput';
import styles from './index.scss';

const SearchInput = ({ searchUsers }) => {
  const [_value, setValue] = useState('');

  const onSearch = (e) => {
    const { target: { value } } = e;
    if (value.length >= 3) {
      searchUsers(value);
    }

    setValue(value);
  };

  return (
    <div className={styles.inputContainer}>
      <MainInput value={_value} onChange={onSearch} />
    </div>
  );
};

SearchInput.propTypes = {
  searchUsers: PropTypes.func.isRequired,
};

export default SearchInput;
