import React from 'react';
import MenuWrapperContainer from 'src/containers/MenuWrapper/MenuWrapperContainer';
import SearchInputContainer from 'src/containers/SearchUsers/SearchInputContainer/index';

import UsersListContainer from 'src/containers/SearchUsers/Users/UsersListContainer/index';

import styles from './index.scss';

const SearchUsers = () => (
  <MenuWrapperContainer showSideBar={false}>
    <div className={styles.searchUsers}>
      <SearchInputContainer />
      <UsersListContainer />
    </div>
  </MenuWrapperContainer>
);

export default SearchUsers;
