import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import styles from './DialogueListItem.scss';
import { ROUTES } from '../../../../common/constants/routes/routes';
import AvatarImage from 'src/components/Common/AvatarImage/AvatarImage';
import ParsedDate from 'src/components/Common/ParsedDate/ParsedDate';

const DialogueListItem = ({ with_id, content, created_at, userInfo }) => (
  <Link to={ROUTES.GET_DIALOGUE_ROUTE(with_id)}>
    <div className={styles.dialogueListItem}>
      <div className={styles.avatarContainer}>
        <AvatarImage imageUrl={userInfo.avatarLink} />
      </div>

      <div className={styles.content}>
        <div className={styles.content__userName}>
          {userInfo.firstName} {userInfo.lastName}
        </div>

        <div className={styles.content__lastMessageContent}>
          {content}
        </div>
      </div>

      <div className={styles.date}>
        <ParsedDate>
          {created_at}
        </ParsedDate>
      </div>
    </div>
  </Link>
);

DialogueListItem.propTypes = {
  with_id: PropTypes.number.isRequired,
  content: PropTypes.string,
  created_at: PropTypes.string.isRequired,
  userInfo: PropTypes.object.isRequired,
};

export default DialogueListItem;
