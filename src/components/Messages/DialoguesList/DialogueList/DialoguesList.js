import React from 'react';
import PropTypes from 'prop-types';
import styles from './DialoguesList.scss';

import Loader from '../../../Common/Loader/Loader';
import DialoguesListItemContainer from 'src/containers/Messages/DialoguesList/DialoguesListItem/DialoguesListItemContainer';
import MenuWrapperContainer from 'src/containers/MenuWrapper/MenuWrapperContainer';
import StartDialogueButtonWrapper from 'src/components/Common/Wrappers/StartDialogueButtonWrapper/StartDialogueButtonWrapper';

class DialoguesList extends React.Component {
  componentDidMount() {
    const { getDialoguesList, dialogues, isPending } = this.props;

    if (!dialogues.lenght && !isPending) {
      getDialoguesList();
    }
  }

  render() {
    const { dialogues, isPending } = this.props;

    return (
      <MenuWrapperContainer>
        <StartDialogueButtonWrapper>
          {isPending ? (
            <Loader />
          ) : (
            <div className={styles.dialoguesListContainer}>
              {dialogues.map((dialogue, index) => (
                <DialoguesListItemContainer
                  index={index}
                  key={`dialogue_list_item:${dialogue.id}`}
                />
              ))}
            </div>
          )}

        </StartDialogueButtonWrapper>
      </MenuWrapperContainer>
    );
  }
}

DialoguesList.propTypes = {
  dialogues: PropTypes.array.isRequired,
  getDialoguesList: PropTypes.func.isRequired,
  isPending: PropTypes.bool.isRequired,
};

export default DialoguesList;
