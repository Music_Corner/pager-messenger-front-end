import React from 'react';
import PropTypes from 'prop-types';
import styles from './Dialogue.scss';

import MessageItemContainer from 'src/containers/Messages/Dialogue/MessageItem/MessageItemContainer';
import Loader from 'src/components/Common/Loader/Loader';
import MenuWrapperContainer from 'src/containers/MenuWrapper/MenuWrapperContainer';
import FormContainer from 'src/containers/Messages/Dialogue/Form/FormContainer';

export default class Dialogue extends React.Component {
  constructor(props) {
    super(props);

    this.messagesContainer = React.createRef();
  }

  componentDidMount() {
    const { getMessages /* messages */ } = this.props;

    // if (!messages.length &&) {
    getMessages(this.getToId());
    // }

    this.scrollMessagesToBottom();
  }

  componentDidUpdate(/* prevProps */) {
    this.scrollMessagesToBottom();
  }

  scrollMessagesToBottom = () => {
    if (this.messagesContainer.current) {
      this.messagesContainer.current.scrollTop = this.messagesContainer.current.scrollHeight;
    }
  }

  getToId = () => {
    const { match } = this.props;
    const { params: { id } } = match;

    return Number(id);
  }

  render() {
    const { messages, isPending } = this.props;

    return (
      <MenuWrapperContainer showSideBar={false}>
        {isPending ? (
          <Loader />
        ) : (
          <div className={styles.dialogueContainer}>
            <div className={styles.messages} ref={this.messagesContainer}>
              {messages.map((message, index) => (
                <MessageItemContainer
                  key={`message_unique_key:${message.id}`}
                  index={index}
                />
              ))}
            </div>

            <div className={styles.form}>
              <FormContainer
                to={this.getToId()}
              />
            </div>
          </div>
        )}
      </MenuWrapperContainer>
    );
  }
}

Dialogue.propTypes = {
  getMessages: PropTypes.func.isRequired,
  messages: PropTypes.array.isRequired,
  isPending: PropTypes.bool.isRequired,
  match: PropTypes.object.isRequired,
};
