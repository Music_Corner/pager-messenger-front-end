import React from 'react';
import PropTypes from 'prop-types';
import styles from './MessageItem.scss';

import AvatarImage from '../../../Common/AvatarImage/AvatarImage';
import ParsedDate from 'src/components/Common/ParsedDate/ParsedDate';

const MessageItem = ({ user, message }) => {
  const {
    created_at, from_id, is_read = false,
    content, avatatar_url = '', attachments = [],
  } = message;

  const isMessageFromMe = from_id === user.id;
  const messageElement = isMessageFromMe
    ? styles.messageContainer__right
    : styles.messageContainer__left;

  const lightModificator = !is_read ? styles.messageContainer_highlighted : '';

  return (
    <div className={`${styles.messageContainer} ${messageElement} ${lightModificator}`}>
      {!isMessageFromMe && (
        <div className={styles.avatarContainer}>
          <AvatarImage imageUrl={avatatar_url} />
        </div>
      )}

      <div className={styles.messageContentContainer}>
        {content && (
          <div className={styles.messageContent} dangerouslySetInnerHTML={{ __html: content }} />
        )}

        {!!attachments.length && (
          <div className={styles.messageAttachments}>
            {attachments.map((attachment, index) => {
              const { type = '', link = '' } = attachment;

              if (type.match('image')) {
                return (
                  <div key={`attachement-${index}`} className={styles.imageAttachmentItem}>
                    <img className={styles.attachmentImage} src={link} />
                  </div>
                );
              }

              return null;
            })}
          </div>
        )}

        <div className={styles.createdAt}>
          <ParsedDate isDetailed={true}>
            {created_at}
          </ParsedDate>
        </div>
      </div>
    </div>
  );
};

MessageItem.propTypes = {
  message: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
};

export default MessageItem;
