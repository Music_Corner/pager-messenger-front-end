import React from 'react';
import PropTypes from 'prop-types';

import DropDownButton from 'src/components/Messages/Dialogue/Form/Attachments/DropDownButton/DropDownButton';
import TextAreaContainer from 'src/containers/Messages/Dialogue/Form/TextArea/TextAreaContainer';
import SubmitButtonContainer from 'src/containers/Messages/Dialogue/Form/SubmitButton/SubmitButtonContainer';
import styles from 'src/components/Messages/Dialogue/Form/Form.scss';
import AttachmentsFormContainer from 'src/containers/Messages/Dialogue/Form/AttachmentsForm/AttachmentsFormContainer';
import AttachmentsPreviewListContainer from 'src/containers/Messages/Dialogue/Form/AttachmenstPreview/AttachmentsPreviewList/AttachmentsPreviewListContainer';

export default class Form extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;

    this.state = {
      modalIsToggled: false,
      modalType: '',
    };
  }

  componentDidMount() {
    const { from, to, changeMessageFromFromTo } = this.props;

    changeMessageFromFromTo(from, to);
  }

  onSendMessage = (e) => {
    const { clearForm, sendMessage } = this.props;

    e.preventDefault();

    sendMessage();
    clearForm();
  }

  openImageModal = () => {
    this.setState(() => ({
      ...this.state,
      modalIsToggled: true,
      modalType: 'image',
    }));
  }

  onCloseModal = () => {
    this.setState(() => ({
      ...this.state,
      modalIsToggled: false,
      modalType: '',
    }));
  }

  render() {
    const { modalIsToggled, modalType } = this.state;

    return (
      <form className={styles.form} onSubmit={this.onSendMessage}>
        <div className={styles.row}>
          <AttachmentsPreviewListContainer />
        </div>

        <div className={styles.row}>
          <DropDownButton onImageFormOpen={this.openImageModal} className={styles.button} />

          <TextAreaContainer />

          <SubmitButtonContainer className={styles.button} />

          <AttachmentsFormContainer
            isToggled={modalIsToggled}
            type={modalType}
            onClose={this.onCloseModal}
          />
        </div>
      </form>
    );
  }
}

Form.propTypes = {
  sendMessage: PropTypes.func.isRequired,
  from: PropTypes.number.isRequired,
  to: PropTypes.number.isRequired,
  changeMessageFromFromTo: PropTypes.func.isRequired,
  clearForm: PropTypes.func.isRequired,
};
