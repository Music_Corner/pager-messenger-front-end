import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import AttachmentsPreviewListItemContainer from 'src/containers/Messages/Dialogue/Form/AttachmenstPreview/AttachmentsPreviewListItem/AttachmentsPreviewListItemContainer';
import styles from './AttachmentsPreviewList.scss';

const AttachmentsPreviewList = ({ attachmentsList }) => (
  <div
    className={classNames(
      styles.attachmentsList,
      { [styles.attachmentsList__fulfilled]: attachmentsList.length }
    )}
  >
    {attachmentsList.map((item, index) => (
      <AttachmentsPreviewListItemContainer
        index={index}
        key={`attachments-list-key:${item.id}`}
      />
    ))}
  </div>
);

AttachmentsPreviewList.propTypes = {
  attachmentsList: PropTypes.array.isRequired,
};

export default AttachmentsPreviewList;
