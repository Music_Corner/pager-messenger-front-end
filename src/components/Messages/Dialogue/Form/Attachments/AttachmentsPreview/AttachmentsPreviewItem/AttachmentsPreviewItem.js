import React from 'react';
import PropTypes from 'prop-types';

import styles from './AttachmentsPreviewItem.scss';

const AttachmentsPreviewItem = ({ onDelete, id, type, file }) => (
  <div className={styles.attachmentItem}>
    <div className={styles.closeContainer} onClick={() => onDelete(id)}>
      <i className="fas fa-times" />
    </div>
    {type === 'image' && (
      <img src={URL.createObjectURL(file)} className={styles.previewImage} />
    )}
  </div>
);

AttachmentsPreviewItem.propTypes = {
  onDelete: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  file: PropTypes.object.isRequired,
};

export default AttachmentsPreviewItem;
