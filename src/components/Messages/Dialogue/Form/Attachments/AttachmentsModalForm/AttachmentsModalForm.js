import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'react-bootstrap';

import styles from './AttachmentsModalForm.scss';
import MainButton from 'src/components/Common/Buttons/MainButton';

const AttachmentsModalForm = ({ isToggled, onFilesUpload, onClose, type }) => (
  <Modal show={isToggled} onHide={onClose}>
    <Modal.Header closeButton>
      <Modal.Title>Upload your files here</Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <div className={styles.form}>
        <label htmlFor="input" className={styles.label}>
          Choose files or drag and drop here
        </label>
        <input
          className={styles.input}
          type="file"
          multiple
          id="input"
          accept={`${type}/*`}
          onChange={({ target: { files } }) => {
            onFilesUpload(files, type);
            onClose();
          }}
        />
      </div>
    </Modal.Body>
    <Modal.Footer>
      <MainButton variant="secondary" onClick={onClose}>
        Close
      </MainButton>
    </Modal.Footer>
  </Modal>
);

AttachmentsModalForm.propTypes = {
  onClose: PropTypes.func.isRequired,
  onFilesUpload: PropTypes.func.isRequired,
  isToggled: PropTypes.bool.isRequired,
  type: PropTypes.string.isRequired,
};

export default AttachmentsModalForm;
