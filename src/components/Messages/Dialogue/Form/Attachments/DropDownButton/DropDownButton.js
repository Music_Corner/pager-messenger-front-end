import React from 'react';
import PropTypes from 'prop-types';
import { Dropdown } from 'react-bootstrap';
import classNames from 'classnames';

import styles from './DropDownButton.scss';

const DropDownButton = ({ onImageFormOpen, className }) => (
  <Dropdown drop="up">
    <Dropdown.Toggle
      className={classNames(className, styles.button__paperclip, styles.dropdownTogle)}
      id="dropdown"
    >
      <i className="fas fa-paperclip" />
    </Dropdown.Toggle>

    <Dropdown.Menu>
      <Dropdown.Item onClick={onImageFormOpen}>
        <i className="fas fa-images" />
        Image
      </Dropdown.Item>
    </Dropdown.Menu>
  </Dropdown>
);

DropDownButton.propTypes = {
  onImageFormOpen: PropTypes.func.isRequired,
  className: PropTypes.string,
};

export default DropDownButton;
