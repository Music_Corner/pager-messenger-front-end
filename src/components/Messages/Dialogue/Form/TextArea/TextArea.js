import React from 'react';
import PropTypes from 'prop-types';

import MainTextArea from "src/components/Common/Inputs/MainTextArea/MainTextArea";
import styles from './TextArea.scss';

const TextArea = ({ value, onChange, clearForm, sendMessage }) => (
  <MainTextArea
    value={value}
    onChange={({ target }) => onChange(target.value)}
    onKeyDown={(e) => {
      const { keyCode, shiftKey } = e;
      if (keyCode === 13 && !shiftKey) {
        sendMessage();
        clearForm();
        e.preventDefault();
      }
    }}
    className={styles.textarea}
    placeholder="Type your message..."
  />
);

TextArea.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  clearForm: PropTypes.func.isRequired,
  sendMessage: PropTypes.func.isRequired,
};

export default TextArea;
