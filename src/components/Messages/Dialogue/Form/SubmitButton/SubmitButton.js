import React from 'react';
import PropTypes from 'prop-types';

import MainButton from 'src/components/Common/Buttons/MainButton';

const SubmitButton = ({ disabled, className }) => (
  <MainButton disabled={disabled} type="submit" className={className}>
    <i className="fas fa-paper-plane" />
  </MainButton>
);

SubmitButton.propTypes = {
  disabled: PropTypes.bool.isRequired,
  className: PropTypes.string,
};

export default SubmitButton;
