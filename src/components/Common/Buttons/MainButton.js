import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import styles from './MainButton.scss';
import { commonPropTypes } from 'src/common/constants/propTypes/propTypesConstants';

const MainButton = (props) => (
  <button {...props} className={classNames(styles.mainButton, props.className)}>
    {props.children}
  </button>
);

MainButton.propTypes = {
  children: commonPropTypes.children,
  className: PropTypes.string,
};

export default MainButton;
