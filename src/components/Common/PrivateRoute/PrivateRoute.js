import React from 'react';
import PropTypes from 'prop-types';
import { Route, withRouter } from 'react-router-dom';

import Loader from 'src/components/Common/Loader/Loader';
import { ROUTES } from 'src/common/constants/routes/routes';
import { TOKEN_NAMES } from 'src/common/constants/tokens/tokens';

class PrivateRoute extends React.Component {
  static propTypes = {
    isPending: PropTypes.bool.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
    checkAuth: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired,
    showError: PropTypes.bool,
  };

  componentDidMount() {
    const {
      checkAuth, history, showError = true,
      isAuthenticated,
    } = this.props;

    const { location: { pathname } } = history;
    const token = localStorage.getItem(TOKEN_NAMES.ACCESS);

    if ((pathname === ROUTES.LOGIN || pathname === ROUTES.SIGN_UP) && !token) {
      return;
    }

    if (!token) {
      history.push(ROUTES.LOGIN);

      return;
    }

    if (!isAuthenticated) {
      checkAuth(history, showError);
    }
  }

  render() {
    const {
      history: { location: { pathname } },
      isPending, isAuthenticated,
    } = this.props;

    if (!isAuthenticated && pathname !== ROUTES.LOGIN && pathname !== ROUTES.SIGN_UP) {
      return null;
    }

    return isPending ? (
      <Loader />
    ) : (
      <Route {...this.props} />
    );
  }
}

export default withRouter(PrivateRoute);
