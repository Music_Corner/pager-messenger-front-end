import React from 'react';
import styles from './InputsGroup.scss';

import { commonPropTypes } from 'src/common/constants/propTypes/propTypesConstants';

const InputsGroup = ({ children }) => (
  <div className={styles.inputsGroup}>
    {children.map(child => ({
      ...child,
      props: {
        ...child.props,
        className: styles.inputs,
      },
    }))}
  </div>
);

InputsGroup.propTypes = {
  children: commonPropTypes.children,
};

export default InputsGroup;
