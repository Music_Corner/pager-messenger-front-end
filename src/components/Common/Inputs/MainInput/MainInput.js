import React from 'react';
import PropTypes from 'prop-types';
import styles from './MainInput.scss';

const MainInput = (props) => (
  <input {...props} className={`${styles.input} ${props.className || ''}`} />
);

MainInput.propTypes = {
  className: PropTypes.string,
};

export default MainInput;
