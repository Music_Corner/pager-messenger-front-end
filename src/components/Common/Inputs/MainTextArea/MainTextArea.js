import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import TextareaAutosize from 'react-textarea-autosize';

import styles from 'src/components/Common/Inputs/MainTextArea/MainTextArea.scss';

const MainTextArea = (props) => (
  <TextareaAutosize
    maxRows={8}
    minRows={1}
    {...props}
    className={classNames(styles.textArea, props.className)}
  />
);

MainTextArea.propTypes = {
  className: PropTypes.string,
};

export default MainTextArea;
