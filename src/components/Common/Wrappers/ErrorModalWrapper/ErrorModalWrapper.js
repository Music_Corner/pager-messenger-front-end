import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button } from 'react-bootstrap';

const ErrorModalWrapper = (props) => {
  const { errorState: { errorModal: { errorMessage, isToggled } }, toggleErrorModal } = props;

  return (
    <>
      <Modal show={isToggled} onHide={toggleErrorModal} animation={false}>
        <Modal.Header closeButton>
          <Modal.Title>Error!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {errorMessage}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={toggleErrorModal}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

ErrorModalWrapper.propTypes = {
  errorState: PropTypes.object.isRequired,
  toggleErrorModal: PropTypes.func.isRequired,
};

export default ErrorModalWrapper;
