import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import { commonPropTypes } from 'src/common/constants/propTypes/propTypesConstants';
import MainButton from 'src/components/Common/Buttons/MainButton';
import { ROUTES } from 'src/common/constants/routes/routes';

import styles from './StartDialogueButtonWrapper.scss';

const StartDialogueButtonWrapper = ({ children, history }) => (
  <>
    {children}
    <div className={styles.floatingButtonWrapper}>
      <MainButton className={styles.floatingButtonWrapper__circle} onClick={() => history.push(ROUTES.START_DIALOGUE)}>
        <i className="fas fa-paper-plane" />
      </MainButton>
    </div>
  </>
);

StartDialogueButtonWrapper.propTypes = {
  children: commonPropTypes.children.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(StartDialogueButtonWrapper);
