import React from 'react';
import Proptypes from 'prop-types';
import classNames from 'classnames';

import styles from './MenuWrapper.scss';
import Header from './Header/Header';
import SideBar from '../../../../containers/Common/SideBar/SideBarContainer';
import withWebsocketPropsContainer from 'src/containers/Common/HOC/Websocket/withWebsocketPropsContainer';

const MenuWrapper = (props) => {
  const {
    title, showSideBar = true, headerExtraButton,
    children, profile,
  } = props;

  const contentClassNames = classNames(styles.mainContent, {
    [styles.mainContent_full_width]: !showSideBar,
  });

  return (
    <div className={styles.menuContainer}>
      <Header
        title={title}
        showGoBackButton={!showSideBar}
        sideBarIsShown={showSideBar}
      >
        {headerExtraButton}
      </Header>

      {showSideBar && (
        <SideBar profile={profile} />
      )}

      <div className={contentClassNames}>
        {children}
      </div>
    </div>
  );
};

MenuWrapper.propTypes = {
  title: Proptypes.string,
  showSideBar: Proptypes.bool,
  headerExtraButton: Proptypes.any,
  children: Proptypes.any,
  profile: Proptypes.object.isRequired,
};

export default withWebsocketPropsContainer(MenuWrapper);
