import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withRouter, Link } from 'react-router-dom';

import styles from './Header.scss';

const Header = (props) => {
  const {
    title, showGoBackButton = false,
    children, history,
  } = props;

  const headerClassNames = classNames(styles.headerContainer, {
    [styles.headerContainer_full_width]: showGoBackButton,
  });

  return (
    <div className={headerClassNames}>
      {showGoBackButton && (
        <Link to="#" onClick={history.goBack}>
          <div className={styles.backButton}>
            <i className="fas fa-arrow-left" />
          </div>
        </Link>
      )}

      <div className={styles.title}>
        {title}
      </div>

      <div className={styles.extraButton}>
        {children}
      </div>
    </div>
  );
};

Header.propTypes = {
  children: PropTypes.any,
  title: PropTypes.string,
  showGoBackButton: PropTypes.bool,
  history: PropTypes.object.isRequired,
};

export default withRouter(Header);
