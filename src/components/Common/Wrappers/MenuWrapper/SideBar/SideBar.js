import React from 'react';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';
import styles from './SideBar.scss';
import AvatarImage from '../../../AvatarImage/AvatarImage';
import { ROUTES } from '../../../../../common/constants/routes/routes';
import MainButton from 'src/components/Common/Buttons/MainButton';

const SideBar = (props) => {
  const {
    profile: { avatatar_url, firstName, lastName },
    logout, history,
  } = props;

  return (
    <div className={styles.sidebarContainer}>
      <Link to={ROUTES.PROFILE}>
        <div className={styles.sidebarHeader}>
          <div className={styles.sidebarHeader__avatarContainer}>
            <AvatarImage imageUrl={avatatar_url} />
          </div>

          <div className={styles.sidebarHeader__username}>
            {firstName} {lastName}
          </div>
        </div>
      </Link>

      <div className={styles.sideBarBody}>
        <ul className={styles.sideBarBody__list}>
          ...
        </ul>
      </div>

      <div className={styles.sideBarFooter}>
        <MainButton onClick={() => logout(history)}>
          Log Out
        </MainButton>
      </div>
    </div>
  );
};

SideBar.propTypes = {
  profile: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired,
};

export default withRouter(SideBar);
