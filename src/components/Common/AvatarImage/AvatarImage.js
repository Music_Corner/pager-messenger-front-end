import React from 'react';
import PropTypes from 'prop-types';
import styles from './AvatarImage.scss';

import defaultAvatar from '../../../../public/images/default-avatar.png';

const AvatarImage = ({ imageUrl }) => (
  <img src={imageUrl || defaultAvatar} className={styles.image} />
);

AvatarImage.propTypes = {
  imageUrl: PropTypes.string,
};

export default AvatarImage;
