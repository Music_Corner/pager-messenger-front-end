import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

const getParsedDate = (dateString, returnDetailed) => {
  const momentDate = moment(dateString);
  const formatedDateString = momentDate.format('YYYY/MM/DD');
  const momentDateFormated = moment(formatedDateString);
  const momentToday = moment();

  if (momentToday.diff(momentDateFormated, 'days') >= 1) {
    const formatString = `DD MMM ${returnDetailed ? 'HH:mm' : ''}`;

    return momentDate.format(formatString);
  }

  return momentDate.format('HH:mm');
};

const ParsedDate = ({ children, isDetailed = false }) => (
  <>
    {getParsedDate(children, isDetailed)}
  </>
);

ParsedDate.propTypes = {
  children: PropTypes.string.isRequired,
  isDetailed: PropTypes.bool,
};

export default ParsedDate;
