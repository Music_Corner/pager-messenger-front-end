import React from 'react';
import PropTypes from 'prop-types';
import Loader from '../../Loader/Loader';

const withWebsocket = (Component) => (
  class extends React.Component {
    static propTypes = {
      profile: PropTypes.object,
      socketConnection: PropTypes.object.isRequired,
      initConnection: PropTypes.func.isRequired,
    };

    componentDidMount() {
      const { socketConnection: { isConnecting, connection }, initConnection } = this.props;

      if (!isConnecting && connection) {
        this.sendInitialSocketMessage();
        return;
      }

      if (!isConnecting && !connection) {
        initConnection(this.sendInitialSocketMessage);
      }
    }

    sendInitialSocketMessage = (_connection) => {
      const { profile, socketConnection: { connection } } = this.props;

      const finalConnection = _connection || connection;

      if (profile) {
        finalConnection.send(profile.id);
      }
    }

    render() {
      const { socketConnection: { isConnecting }, profile } = this.props;

      if (!profile) {
        return null;
      }

      return isConnecting ? <Loader /> : (
        <Component {...this.props} />
      );
    }
  }
);

export default withWebsocket;
