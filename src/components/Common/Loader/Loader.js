import React from 'react';
import styles from './Loader.scss';

const Loader = () => (
  <div className={styles.loader}>
    <div className={styles['lds-dual-ring']} />
  </div>
);

export default Loader;
