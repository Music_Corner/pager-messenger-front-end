/* eslint-disable react/display-name */
import React from 'react';
import PropTypes from 'prop-types';

import styles from './AuthFormWrapper.scss';
import logo from '../../../../public/images/logo.png';

const AuthFormWrapper = (props) => {
  const { children, title } = props;

  return (
    <div className={styles.signUpContainer}>
      <div className={styles.signUpLogo}>
        <img className={styles.logoImg} src={logo} alt="" />
      </div>

      <div className={styles.title}>{title}</div>

      {children}
    </div>
  );
};

AuthFormWrapper.propTypes = {
  title: PropTypes.string,
  children: PropTypes.any,
};

export default AuthFormWrapper;
