import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

class EmailVerification extends React.Component {
  componentDidMount() {
    const { fetchVerify, match: { params }, history } = this.props;

    if (params && params.verificationHash) {
      fetchVerify({ hash: params.verificationHash, history });
    }
  }

  render() {
    return (
      <div>Loading...</div>
    );
  }
}

EmailVerification.propTypes = {
  fetchVerify: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired,
};

export default withRouter(EmailVerification);
