import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import MainInput from '../../Common/Inputs/MainInput/MainInput';
import AuthFormWrapper from '../AuthFormWrapper/AuthFormWrapper';
import styles from './Login.scss';
import MainButton from '../../Common/Buttons/MainButton';

const Login = (props) => {
  const [formState, onChangeInputValue] = useState({ login: '', password: '' });

  const { login = '', password = '' } = formState;

  const changeInputValue = (name) => ({ target: { value } }) => {
    onChangeInputValue({ ...formState, [name]: value });
  };

  const onSubmit = (e) => {
    e.preventDefault();

    const { submitForm, history } = props;

    submitForm(formState, history);
  };

  const isValid = login && password;

  return (
    <AuthFormWrapper title="Login:">
      <form className={styles.form} onSubmit={onSubmit}>
        <div className={styles.formInputs}>
          <MainInput
            placeholder="Login"
            onChange={changeInputValue('login')}
            value={login}
          />
          <MainInput
            placeholder="Password"
            type="password"
            onChange={changeInputValue('password')}
            value={password}
          />
        </div>

        <div className={styles.formSubmit}>
          <MainButton disabled={!isValid} type="submit">Login</MainButton>
        </div>
      </form>
      <Link to="/sign-up">Don't have account yet?</Link>
    </AuthFormWrapper>
  );
};

Login.propTypes = {
  submitForm: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

export default Login;
