import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styles from './SignUp.scss';
import MainInput from '../../Common/Inputs/MainInput/MainInput';
import MainButton from '../../Common/Buttons/MainButton';
import AuthFormWrapper from '../AuthFormWrapper/AuthFormWrapper';
import InputsGroup from '../../Common/Inputs/InputsGroup/InputsGroup';

export default class SignUp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      login: '',
      password: '',
      confirmPassword: '',
      firstName: '',
      lastName: '',
    };
  }

  validate = () => {
    const {
      email, firstName, password,
      confirmPassword, lastName,
    } = this.state;

    return email && firstName && lastName && password && confirmPassword;
  }

  onSubmit = (e) => {
    e.preventDefault();

    const { submitForm } = this.props;

    if (this.validate()) {
      submitForm(this.state);
    }
  }

  changeInputValue = (inputName) => ({ target: { value } }) => {
    this.setState(() => ({
      ...this.state,
      [inputName]: value,
    }));
  }

  render() {
    const {
      firstName, lastName, email,
      login, password, confirmPassword,
    } = this.state;

    const isValid = this.validate();

    return (
      <AuthFormWrapper title="Sign Up:">
        <form className={styles.form} onSubmit={this.onSubmit}>
          <div className={styles.formInputs}>
            <MainInput
              placeholder="Email"
              type="email"
              onChange={this.changeInputValue('email')}
              value={email}
            />
            <MainInput
              placeholder="Login"
              onChange={this.changeInputValue('login')}
              value={login}
            />
            {/* <div className={styles.inputRow}> */}
            <InputsGroup>
              <MainInput
                placeholder="First name"
                onChange={this.changeInputValue('firstName')}
                value={firstName}
              />
              <MainInput
                placeholder="Last name"
                onChange={this.changeInputValue('lastName')}
                value={lastName}
              />
            </InputsGroup>
            {/* </div> */}
            <MainInput
              placeholder="Password"
              type="password"
              onChange={this.changeInputValue('password')}
              value={password}
            />
            <MainInput
              placeholder="Password confirmation"
              type="password"
              onChange={this.changeInputValue('confirmPassword')}
              value={confirmPassword}
            />
          </div>

          <div className={styles.formSubmit}>
            <MainButton disabled={!isValid} type="submit">Sign Up</MainButton>
          </div>
        </form>

        <Link to="/login">Already have an account?</Link>
      </AuthFormWrapper>
    );
  }
}

SignUp.propTypes = {
  submitForm: PropTypes.func.isRequired,
};
