import { createSelector } from 'reselect';

import usersListSelector from 'src/selectors/usersList/usersListSelector';

const getUsersListState = (state, { index }) => usersListSelector(state).data[index];

const usersListItemSelector = createSelector(
  [getUsersListState],
  user => user,
);

export default usersListItemSelector;
