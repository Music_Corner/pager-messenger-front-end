import { createSelector } from 'reselect';

const getUsersListState = state => state.users;

const usersListSelector = createSelector(
  [getUsersListState],
  usersState => usersState,
);

export default usersListSelector;
