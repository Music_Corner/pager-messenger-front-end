import { createSelector } from 'reselect';

const getSocketConnection = state => state.socketConnection;

const socketConnectionSelector = createSelector(
  [getSocketConnection],
  socketConnection => socketConnection,
);

export default socketConnectionSelector;
