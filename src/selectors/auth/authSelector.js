import { createSelector } from 'reselect';

const getAuth = state => state.auth;

const authSelector = createSelector(
  [getAuth],
  auth => auth,
);

export default authSelector;
