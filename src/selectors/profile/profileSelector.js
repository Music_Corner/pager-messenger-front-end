import { createSelector } from 'reselect';

const getProfile = state => state.profile;

const profileSelector = createSelector(
  [getProfile],
  profile => profile,
);

export default profileSelector;
