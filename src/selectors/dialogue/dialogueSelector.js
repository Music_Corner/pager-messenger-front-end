import { createSelector } from 'reselect';

const getDialogue = state => state.messages;

const dialogueSelector = createSelector(
  [getDialogue],
  ({ isPending, data }) => ({ messages: data, isPending }),
);

export default dialogueSelector;
