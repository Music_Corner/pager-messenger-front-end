import { createSelector } from 'reselect';

const getDialogueFormDataToSend = state => state.messages.form.dataToSend;

const dialogueFormSelector = createSelector(
  [getDialogueFormDataToSend],
  (data) => ({ ...data, submitDisabled: !data.content && !data.attachments.length }),
);

export default dialogueFormSelector;
