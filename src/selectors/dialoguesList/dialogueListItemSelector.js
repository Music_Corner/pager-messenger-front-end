import { createSelector } from 'reselect';

import { dialoguesListSelector } from "src/selectors/dialoguesList/dialoguesListSelector"

const getDialoguesListItem = (state, { index }) => dialoguesListSelector(state)[index];

const dialoguesListItemSelector = createSelector(
  [getDialoguesListItem],
  dialoguesListItem => dialoguesListItem,
);

export default dialoguesListItemSelector;
