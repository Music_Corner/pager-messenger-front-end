import { createSelector } from 'reselect';

const getDialoguesList = state => state.dialogues.data;

export const dialoguesListSelector = createSelector(
  [getDialoguesList],
  (dialoguesList) => dialoguesList,
);
