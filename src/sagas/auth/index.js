import { call, put, takeEvery } from 'redux-saga/effects';
import profileApi from '../../api/profile/index';
import errorHandledSaga from '../utils/errorHandledSaga';
import { emailVerificationActions, EMAIL_VERIFICATION_ACTIONS } from '../../actions/auth/emailVerification';
import { signUpFetchActions, SIGN_UP_ACTIONS } from '../../actions/auth/signUp';
import { checkAuthActions, CHECK_AUTH_ACTIONS } from '../../actions/auth/checkAuth';
import { fetchLoginActions, FETCH_LOGIN_ACTIONS } from '../../actions/auth/login';
import { logoutActions, LOGOUT_ACTIONS } from '../../actions/auth/logout';
import { ROUTES } from 'src/common/constants/routes/routes';
import { TOKEN_NAMES, removeTokens, saveTokens } from 'src/common/constants/tokens/tokens';

function* fetchSignUp(action) {
  try {
    const signUpResponse = yield call(profileApi.postSignUp, action.payload);

    yield put(signUpFetchActions.success(signUpResponse.data.data));
    alert('Registered successfully, please verify your email to get acces to the site');
  } catch (error) {
    yield put(signUpFetchActions.error(error.error));
    throw error;
  }
}

function* verifyEmail({ payload }) {
  const { history, hash } = payload;

  try {
    const verificationResponse = yield call(profileApi.verifyEmail, hash);

    yield put(emailVerificationActions.success(verificationResponse.data.data));
    yield saveTokens(verificationResponse.data.data);
    yield history.push(ROUTES.HOME);
  } catch (error) {
    yield put(emailVerificationActions.error(error.error));
    yield history.push(ROUTES.SIGN_UP);
  }
}

function* refreshToken(history) {
  try {
    const refreshAuthResult = yield call(profileApi.checkAuth, true);

    yield put(checkAuthActions.success(refreshAuthResult.data.data));
    yield history.push(ROUTES.HOME);
    yield saveTokens(refreshAuthResult.data.data);
  } catch (refreshErr) {
    yield put(checkAuthActions.error(refreshErr.error));
    yield history.push(ROUTES.LOGIN);
    yield removeTokens();

    throw refreshErr;
  }
}

function* checkAuth(action) {
  const { history, showError } = action;
  const { location: { pathname } } = history;

  try {
    const checkAuthResult = yield call(profileApi.checkAuth);

    yield put(checkAuthActions.success(checkAuthResult.data.data));
    yield saveTokens(checkAuthResult.data.data);

    if (pathname === ROUTES.LOGIN || pathname === ROUTES.SIGN_UP) {
      yield history.push(ROUTES.HOME);
    }
  } catch (e) {
    if (e.status === 401) {
      yield refreshToken(history);

      return;
    }

    if (showError) {
      yield history.push(ROUTES.LOGIN);
      yield put(checkAuthActions.error(e.error.error));
      // throw e.error.error;
    }
  }
}

function* login(action) {
  console.log('loggin in');
  const { history, payload } = action;

  try {
    const result = yield call(profileApi.login, payload);

    yield put(fetchLoginActions.success(result.data.data));
    yield history.push(ROUTES.HOME);
    yield saveTokens(result.data.data);
  } catch (e) {
    yield put(fetchLoginActions.error(e.error));
    throw e;
  }
}

function* logout(action) {
  const { history } = action;

  const result = yield call(profileApi.logout);

  yield put(logoutActions.success(result.data.data));
  yield localStorage.removeItem(TOKEN_NAMES.ACCESS);
  yield localStorage.removeItem(TOKEN_NAMES.REFRESH);
  yield history.push(ROUTES.LOGIN);
}

export default function* authSagas() {
  yield takeEvery(SIGN_UP_ACTIONS.REQUEST, errorHandledSaga(fetchSignUp));
  yield takeEvery(EMAIL_VERIFICATION_ACTIONS.REQUEST, verifyEmail);
  yield takeEvery(CHECK_AUTH_ACTIONS.REQUEST, checkAuth);
  yield takeEvery(FETCH_LOGIN_ACTIONS.REQUEST, errorHandledSaga(login));
  yield takeEvery(LOGOUT_ACTIONS.REQUEST, logout);
}
