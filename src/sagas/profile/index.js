import { put, takeEvery } from 'redux-saga/effects';
import { profileActions } from 'src/actions/profile/profileActions';
import { EMAIL_VERIFICATION_ACTIONS } from 'src/actions/auth/emailVerification';
import { LOGOUT_ACTIONS } from 'src/actions/auth/logout';
import { FETCH_LOGIN_ACTIONS } from 'src/actions/auth/login';
import { CHECK_AUTH_ACTIONS } from 'src/actions/auth/checkAuth';

function* fulfillProfile({ payload }) {
  yield put(profileActions.fulfillProfileInfo(payload));
}

function* clearProfile() {
  yield put(profileActions.clearProfileInfo());
}

function* profileSagas() {
  yield takeEvery(EMAIL_VERIFICATION_ACTIONS.SUCCESS, fulfillProfile);
  yield takeEvery(CHECK_AUTH_ACTIONS.SUCCESS, fulfillProfile);
  yield takeEvery(FETCH_LOGIN_ACTIONS.SUCCESS, fulfillProfile);
  yield takeEvery(LOGOUT_ACTIONS.REQUEST, clearProfile);
}

export default profileSagas;
