import { debounce, call, put } from 'redux-saga/effects';

import { getUsersActions, GET_USERS_ACTIONS_NAMES } from 'src/actions/users/getUsersActions';
import { usersApi } from 'src/api/users/index';

function* getUsers({ payload }) {
  try {
    const data = yield call(usersApi.getUsers, { search: payload, page: 0 });

    yield put(getUsersActions.success(data.data.data));
  } catch (error) {
    yield put(getUsersActions.error(error));
  }
}

export default function* usersSagas() {
  yield debounce(800, GET_USERS_ACTIONS_NAMES.REQUEST, getUsers);
}
