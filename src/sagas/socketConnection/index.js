import { takeEvery, call, put, take } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import initWebSocket from '../../common/webSocket/connection';
import { socketConnectionActions, SOCKET_CONNECTION } from '../../actions/socketConnection/socketConnectionActions';
import errorHandledSaga from '../utils/errorHandledSaga';
import onMessageAction from '../../actions/messages/onMessage';

let connection = null;

const connect = () => new Promise((resolve, reject) => {
  connection = initWebSocket();
  console.log('open');

  connection.onopen = () => {
    resolve(connection);
  };

  connection.onerror = (e) => {
    reject(e);
  };
});

function* initSocketConnection(action) {
  try {
    if (!connection) {
      connection = yield call(connect);
    }

    yield put(socketConnectionActions.success(connection));

    if (action && action.payload) {
      const callBack = action.payload;

      yield callBack(connection);
    }
  } catch (e) {
    yield put(socketConnectionActions.error(e));
    throw e;
  }
}

const socketEventChannel = (connection) => eventChannel(emit => {
  connection.onmessage = (event) => (
    emit(JSON.parse(event.data))
  );

  connection.onerror = (e) => (
    emit(e)
  );

  connection.onclose = () => (
    emit(null)
  );

  return () => socketConnectionActions.success(null);
});

function* handleEventChannels() {
  try {
    if (!connection) {
      connection = yield call(connect);
    }

    const channel = yield call(socketEventChannel, connection);

    while (true) {
      const socketPayload = yield take(channel);
      yield put(onMessageAction(socketPayload));
    }
  } catch (e) {
    console.log(e);
    throw e;
  }
}


function* socketConnectionSagas() {
  yield takeEvery(SOCKET_CONNECTION.REQUEST, errorHandledSaga(initSocketConnection));
  yield takeEvery(SOCKET_CONNECTION.SUCCESS, handleEventChannels);
}

export default socketConnectionSagas;
