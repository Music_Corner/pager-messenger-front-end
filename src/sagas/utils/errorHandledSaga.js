import { call, put } from 'redux-saga/effects';
import { toggleErrorModal } from '../../actions/common/errorActions';
import { checkAuthActions } from '../../actions/auth/checkAuth';

export default function errorHandledSaga(saga) {
  return function* (...args) {
    try {
      return yield call(saga, ...args);
    } catch (e) {
      console.log(e);
      if (e.status === 401) {
        yield put(checkAuthActions.error(e));
      }

      return yield put(toggleErrorModal(`${e.error}`));
    }
  }
}
