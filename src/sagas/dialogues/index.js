import { call, put, takeEvery } from 'redux-saga/effects';
import { dialoguesApi } from '../../api/dialogues/index';
import { fetchDialoguesListActions, FETCH_DIALOGUES_LIST_ACTIONS } from '../../actions/dialogues/getDialoguesList';
import errorHandledSaga from '../utils/errorHandledSaga';

function* getDialoguesList(action) {
  const { payload } = action;

  try {
    const res = yield call(dialoguesApi.get, payload);

    yield put(fetchDialoguesListActions.success(res));
  } catch (error) {
    yield put(fetchDialoguesListActions.error(error.error));
    throw error.error;
  }
}

export default function* dialoguesSagas() {
  yield takeEvery(FETCH_DIALOGUES_LIST_ACTIONS.REQUEST, errorHandledSaga(getDialoguesList));
}
