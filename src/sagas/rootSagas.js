import { all } from 'redux-saga/effects';

import authSagas from 'src/sagas/auth/index';
import profileSagas from './profile/index';
import messagesSagas from './messages/index';
import socketConnectionSagas from './socketConnection/index';
import dialoguesSagas from './dialogues/index';
import usersSagas from 'src/sagas/users/index';

export default function* rootSagas() {
  yield all([
    authSagas(),
    profileSagas(),
    messagesSagas(),
    socketConnectionSagas(),
    dialoguesSagas(),
    usersSagas(),
  ]);
}
