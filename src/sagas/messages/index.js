import { call, takeEvery, put } from 'redux-saga/effects';

import { menuWrapperActions } from 'src/actions/ui/menuWrapperActions';
import messagesApi from '../../api/messages/index';
import { GET_MESSAGES_ACTIONS, getMessagesActions } from '../../actions/messages/getMessages';
import errorHandledSaga from '../utils/errorHandledSaga';
import { sendMessageActions, SEND_MESSAGE_ACTIONS } from 'src/actions/messages/sendMessage';
import { HEADER_TITLES_CONSTANTS } from 'src/common/constants/ui/ui';

function* getMessages({ payload }) {
  try {
    const results = yield call(messagesApi.getMessages, payload);
    const { data: { data: { messages, collocutorInfo: { firstName, lastName } } } } = results;

    yield put(getMessagesActions.success(messages));
    yield put(menuWrapperActions.setHeaderTitle(HEADER_TITLES_CONSTANTS.GET_DIALOGUE_TITLE(firstName, lastName)));
  } catch (e) {
    yield put(getMessagesActions.error(e.error));
    throw e;
  }
}

function* sendMessage({ payload }) {
  try {
    const formData = new FormData();

    Object.keys(payload).forEach((key) => {
      const value = payload[key];

      if (key === 'attachments') {
        value.forEach((attachment, index) => {
          formData.append('file', attachment.file);
        });

        return;
      }

      formData.append(key, value);
    });

    const results = yield call(messagesApi.postMessage, formData);
    const { data: { data } } = results;

    yield put(sendMessageActions.success(data));
  } catch (e) {
    yield put(sendMessageActions.error(e.error));

    throw e;
  }
}

export default function* messagesSagas() {
  yield takeEvery(GET_MESSAGES_ACTIONS.REQUEST, errorHandledSaga(getMessages));
  yield takeEvery(SEND_MESSAGE_ACTIONS.REQUEST, errorHandledSaga(sendMessage));
}
