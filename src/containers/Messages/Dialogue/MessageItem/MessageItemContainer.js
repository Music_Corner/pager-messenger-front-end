import { connect } from 'react-redux';
import MessageItem from '../../../../components/Messages/Dialogue/MessageItem/MessageItem';
import profileSelector from 'src/selectors/profile/profileSelector';

const mapStateToProps = (state, { index }) => ({
  user: profileSelector(state),
  message: state.messages.data[index],
});

export default connect(mapStateToProps, {})(MessageItem);
