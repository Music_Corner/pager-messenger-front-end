import { connect } from 'react-redux';
import { getMessagesActions } from '../../../../actions/messages/getMessages';
import Dialogue from '../../../../components/Messages/Dialogue/Dialogue/Dialogue';
import dialogueSelector from 'src/selectors/dialogue/dialogueSelector';

const mapStateToProps = dialogueSelector;

const mapDispatchToProps = {
  getMessages: getMessagesActions.request,
};

export default connect(mapStateToProps, mapDispatchToProps)(Dialogue);
