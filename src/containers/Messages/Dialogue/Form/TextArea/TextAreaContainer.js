import { connect } from "react-redux";

import { changeMessageFormActions } from "src/actions/messages/changeFormActions";
import { sendMessageActions } from "src/actions/messages/sendMessage";
import TextArea from "src/components/Messages/Dialogue/Form/TextArea/TextArea";
import dialogueFormSelector from "src/selectors/dialogue/dialogueFormSelector";

const mapStateToProps = (state) => ({
  value: dialogueFormSelector(state).content,
});

const mapDispatchToProps = {
  onChange: changeMessageFormActions.content,
  clearForm: changeMessageFormActions.clear,
  sendMessage: sendMessageActions.request,
};

export default connect(mapStateToProps, mapDispatchToProps)(TextArea);
