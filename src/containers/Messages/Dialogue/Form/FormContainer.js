import { connect } from 'react-redux';

import { sendMessageActions } from 'src/actions/messages/sendMessage';
import Form from 'src/components/Messages/Dialogue/Form/Form';
import profileSelector from 'src/selectors/profile/profileSelector';
import { changeMessageFormActions } from 'src/actions/messages/changeFormActions';

const mapStateToProps = (state) => ({
  from: profileSelector(state).id,
});

const mapDispatchToProps = {
  sendMessage: sendMessageActions.request,
  clearForm: changeMessageFormActions.clear,
  changeMessageFromFromTo: changeMessageFormActions.fromTo,
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);
