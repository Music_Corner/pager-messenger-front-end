import { connect } from "react-redux";

import { changeMessageFormActions } from "src/actions/messages/changeFormActions";
import AttachmentsModalForm from "src/components/Messages/Dialogue/Form/Attachments/AttachmentsModalForm/AttachmentsModalForm";

const mapStateToProps = () => ({});

const mapDispatchToProps = {
  onFilesUpload: changeMessageFormActions.addAttachments,
};

export default connect(mapStateToProps, mapDispatchToProps)(AttachmentsModalForm);
