import { connect } from 'react-redux';

import dialogueFormSelector from 'src/selectors/dialogue/dialogueFormSelector';
import { changeMessageFormActions } from 'src/actions/messages/changeFormActions';
import AttachmentsPreviewItem from 'src/components/Messages/Dialogue/Form/Attachments/AttachmentsPreview/AttachmentsPreviewItem/AttachmentsPreviewItem';

const mapStateToProps = (state, { index }) => ({
  ...dialogueFormSelector(state).attachments[index],
});

const mapDispatchToProps = {
  onDelete: changeMessageFormActions.deleteAttachment,
};

export default connect(mapStateToProps, mapDispatchToProps)(AttachmentsPreviewItem);
