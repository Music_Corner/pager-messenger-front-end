import dialogueFormSelector from 'src/selectors/dialogue/dialogueFormSelector';
import { connect } from 'react-redux';
import AttachmentsPreviewList from 'src/components/Messages/Dialogue/Form/Attachments/AttachmentsPreview/AttachmentsPreviewList/AttachmentsPreviewList';

const mapStateToProps = (state) => ({
  attachmentsList: dialogueFormSelector(state).attachments,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(AttachmentsPreviewList);
