import { connect } from "react-redux";

import SubmitButton from "src/components/Messages/Dialogue/Form/SubmitButton/SubmitButton";
import dialogueFormSelector from "src/selectors/dialogue/dialogueFormSelector";

const mapStateToProps = state => ({
  disabled: dialogueFormSelector(state).submitDisabled,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(SubmitButton);
