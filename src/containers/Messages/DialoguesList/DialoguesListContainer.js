import { connect } from 'react-redux';
import { fetchDialoguesListActions } from '../../../actions/dialogues/getDialoguesList';
import DialoguesList from '../../../components/Messages/DialoguesList/DialogueList/DialoguesList';
import { dialoguesListSelector } from '../../../selectors/dialoguesList/dialoguesListSelector';

const mapStateToProps = (state) => ({
  dialogues: dialoguesListSelector(state),
  isPending: state.dialogues.isPending,
});

const mapDispatchToProps = {
  getDialoguesList: fetchDialoguesListActions.request,
};

export default connect(mapStateToProps, mapDispatchToProps)(DialoguesList);
