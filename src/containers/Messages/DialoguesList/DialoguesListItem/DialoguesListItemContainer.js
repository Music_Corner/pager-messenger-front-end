import { connect } from 'react-redux';

import DialogueListItem from '../../../../components/Messages/DialoguesList/DialoguesListItem/DialoguesListItem';
import dialoguesListItemSelector from 'src/selectors/dialoguesList/dialogueListItemSelector';

const mapStateToProps = dialoguesListItemSelector;

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(DialogueListItem);
