import { connect } from 'react-redux';
import SignUp from '../../../components/Profile/SignUp/SignUp';
import { signUpFetchActions } from '../../../actions/auth/signUp';

const actions = {
  submitForm: signUpFetchActions.request,
};

const mapStateToProps = (state) => ({
  profile: state.profile,
});

export default connect(mapStateToProps, actions)(SignUp);
