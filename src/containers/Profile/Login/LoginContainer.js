import { connect } from 'react-redux';
import { fetchLoginActions } from '../../../actions/auth/login';
import Login from '../../../components/Profile/Login/Login';

const mapStateToProps = () => ({});

const mapDispatchToProps = {
  submitForm: fetchLoginActions.request,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
