import { connect } from 'react-redux';
import { emailVerificationActions } from '../../../actions/auth/emailVerification';
import EmailVerification from '../../../components/Profile/EmailVerification/EmailVerification';

const mapDispatchToProps = {
  fetchVerify: emailVerificationActions.request,
};

const mapStateToProps = (state) => ({
  profile: state.profile,
});

export default connect(mapStateToProps, mapDispatchToProps)(EmailVerification);
