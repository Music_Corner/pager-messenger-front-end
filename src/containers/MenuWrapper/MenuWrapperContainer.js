import { connect } from 'react-redux';

import MenuWrapper from 'src/components/Common/Wrappers/MenuWrapper/MenuWrapper';
import { menuWrapperActions } from 'src/actions/ui/menuWrapperActions';

const mapStateToProps = ({ ui: { header: { title } } }, ownProps) => ({
  title: ownProps.title || title,
});

const mapDispatchToProps = {
  setHeaderTitle: menuWrapperActions.setHeaderTitle,
};

export default connect(mapStateToProps, mapDispatchToProps)(MenuWrapper);
