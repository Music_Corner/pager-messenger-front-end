import { connect } from 'react-redux';

import { getUsersActions } from 'src/actions/users/getUsersActions';
import SearchInput from 'src/components/SearchUsers/SearchInput/index';

const mapStateToProps = () => ({ });

const mapDispatchToProps = {
  searchUsers: getUsersActions.request,
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchInput);
