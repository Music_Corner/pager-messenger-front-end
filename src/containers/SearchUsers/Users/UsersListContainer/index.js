import { connect } from 'react-redux';
import { menuWrapperActions } from 'src/actions/ui/menuWrapperActions';
import { HEADER_TITLES_CONSTANTS } from 'src/common/constants/ui/ui';

import UsersList from 'src/components/SearchUsers/Users/UsersList/index';
import usersListSelector from 'src/selectors/usersList/usersListSelector';

const mapStateToProps = usersListSelector;

const mapDispatchToProps = {
  setHeaderTitle: () => menuWrapperActions.setHeaderTitle(HEADER_TITLES_CONSTANTS.NEW_DIALOGUE),
};

export default connect(mapStateToProps, mapDispatchToProps)(UsersList);
