import { connect } from 'react-redux';

import UsersListItem from 'src/components/SearchUsers/Users/UsersListItem/index';
import usersListItemSelector from 'src/selectors/user/usersListSelector';

const mapStateToProps = usersListItemSelector;

export default connect(mapStateToProps)(UsersListItem);
