import { connect } from 'react-redux';
import { compose } from 'recompose';

import withWebsocket from '../../../../components/Common/HOC/Websocket/withWebsocket';
import { socketConnectionActions } from '../../../../actions/socketConnection/socketConnectionActions';
import socketConnectionSelector from 'src/selectors/socketConnection/socketConnectionSelector';
import profileSelector from 'src/selectors/profile/profileSelector';

const mapStateToProps = (state) => ({
  socketConnection: socketConnectionSelector(state),
  profile: profileSelector(state),
});

const mapDispatchToProps = {
  initConnection: socketConnectionActions.request,
};

export default compose(connect(
  mapStateToProps, mapDispatchToProps,
), withWebsocket);
