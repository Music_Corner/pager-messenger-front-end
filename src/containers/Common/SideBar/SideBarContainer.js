import { connect } from 'react-redux';
import { logoutActions } from '../../../actions/auth/logout';
import SideBar from '../../../components/Common/Wrappers/MenuWrapper/SideBar/SideBar';

const mapStateToProps = () => ({});

const mapDispatchToProps = {
  logout: logoutActions.request,
};

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
