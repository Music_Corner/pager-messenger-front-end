import { connect } from 'react-redux';
import PrivateRoute from 'src/components/Common/PrivateRoute/PrivateRoute';
import { checkAuthActions } from 'src/actions/auth/checkAuth';
import authSelector from 'src/selectors/auth/authSelector';

const mapStateToProps = authSelector;

const mapDispatchToProps = {
  checkAuth: checkAuthActions.request,
};

export default connect(mapStateToProps, mapDispatchToProps)(PrivateRoute);
