import { connect } from 'react-redux';
import ErrorModalWrapper from '../../../components/Common/Wrappers/ErrorModalWrapper/ErrorModalWrapper';
import { toggleErrorModal } from 'src/actions/common/errorActions';

const mapStateToProps = (state) => ({
  errorState: state.error,
});

const mapDispatchToProps = {
  toggleErrorModal,
};

export default connect(mapStateToProps, mapDispatchToProps)(ErrorModalWrapper);
