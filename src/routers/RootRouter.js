import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import SignUpContainer from '../containers/Profile/SignUp/SignUpContainer';
import EmailVerificationContainer from '../containers/Profile/EmailVerification/EmailVerificationContainer';
import LoginContainer from '../containers/Profile/Login/LoginContainer';
import DialogueContainer from '../containers/Messages/Dialogue/Dialogue/DialogueContainer';
import { ROUTES } from '../common/constants/routes/routes';
import DialoguesListContainer from '../containers/Messages/DialoguesList/DialoguesListContainer';
import PrivateRouteContainer from 'src/containers/Common/PrivateRoute/PrivateRouteContainer';
import SearchUsers from 'src/components/SearchUsers/index';

export class RootRouter extends React.Component {
  render() {
    const signUpRender = (props) => <SignUpContainer {...props} showError={false} />;
    const loginRender = (props) => <LoginContainer {...props} showError={false} />;

    return (
      <BrowserRouter>
        <Switch>
          <PrivateRouteContainer path={ROUTES.SIGN_UP} render={signUpRender} />
          <PrivateRouteContainer path={ROUTES.LOGIN} render={loginRender} />
          <Route
            path={ROUTES.EMAIL_VERIFICATION}
            component={EmailVerificationContainer}
          />
          <PrivateRouteContainer path={ROUTES.DIALOGUE} component={DialogueContainer} />
          <PrivateRouteContainer path={ROUTES.HOME} component={DialoguesListContainer} />
          <PrivateRouteContainer path={ROUTES.START_DIALOGUE} component={SearchUsers} />

          <PrivateRouteContainer render={() => <Redirect to={ROUTES.HOME} />} />
        </Switch>
      </BrowserRouter>
    );
  }
}
